# Activitat Rsync Ivan Pardo


NOTA: tenir creat l'usuari unix01 amb la contrasenya unix01

EL primer de tot serà engegar 1 pcs,un ldap, perque utilitzarem el servidor AWS

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net mynet -p "389:389" -d ivanpardo/ldap23:latest
docker run --rm --name ssh.edt.org -h ssh.edt.org --net mynet -p "2022:22" -d ivanpardo/ssh23:base

```
En les dues maquines instalarem els paquets:

```
apt-get install Rsync -y
apt-get install openssh-client -y
apt-get install openssh-server -y
```
Crear usuaris unix01 i unix02 
```
useradd -m -d /home/unix01 -s /bin/bash unix01
passwd unix01
```
## Fitxer de configuració Rsync dins del ssh

editem el fitxer /etc/rsyncd.conf
```
[man]
	path = /usr/share/man
	comment = mans del sistema
[doc]
	path = /usr/share/doc
	comment = documentacio del sistema
[practiques_unix01]
	path = /var/tmp/practiques/
	comment = permisos d'escriptura i de lectura
	read only = no
	auth users = unix03, unix01, remot
	secrets file = /etc/rsyncd.secrets

[practiques_unix02]
	path = /var/tmp/practiques/
	comment = permisos nomes de lectura
	read only = yes
	auth users = unix02
	secrets file = /etc/rsyncd.secrets
```
Per a la llista d'usuaris modificarem el fitxer /etc/rsyncd.secrets
```
unix01:unix01
unix02:unix02
unix03:unix03
remot:remot
```
I li posarem els permisos 600 amb chmod

Crear carpeta /var/tmp/practiques amb la comanda: 
```
mkdir /var/tmp/practiques
(ssh server)chmod 777 /var/tmp/practiques
```
NOTA: HAS D'ANCENDRE EL DIMONI
```
rsync --daemon
```
## Activitats

### Ex1:
```
(unix01)rsync -a /home/unix01 172.20.0.3::practiques_unix01

```
### Ex 2:
```
(unix01)rsync  172.20.0.3::practiques_unix01
```
### Ex 3:
NOTA: aquest usuari no podra copiar el seu home perque nomes te permisos de lectura
```
(unix02)rsync -avz /home/unix02 172.20.0.2::practiques_unix02
```

### Ex 4:
```
mkdir /home/unix02/.ssh
chmod 777 /home/unix02/.ssh
en el server fer chmod 777 /var/tmp/practiques/unix01/.ssh
rsync -avz 172.20.0.3::practiques_unix02/unix01/.ssh .

```

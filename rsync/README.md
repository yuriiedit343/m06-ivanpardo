# Practica Rsync

### Creacio de dos carpetes de prova:

```
mkdir prova1
mkdir prova2
touch prova1/file{1..200}
```

Així tenim dos directoris, i el directori prova 1 amb 200 files per poder fer proves

### Realitzar sincronització:

´´´
rsync -r prova1/ prova2
´´´

## Local - Local

Per comprobar la sincronització editarem el nom d'un fitxer, en aquest cas posarem de nom ivan al file1 del prova1
```
rsync -av dir1/ dir2
```
Ara amb un ls podem veure com el nom del fitxer s'ha modificat

```
ls -l prova2
```

## Local - Remot

En aquest pas hem d'instalar un docker compose per simular un servidor.

```
version: "2"
services:
  ldap:
	image: ivanpardo/ldap23:latest
	container_name: ldap.edt.org
	hostname: ldap.edt.org
	ports:
  	- "389:389"
	networks:
  	- mynet
  ssh:
	image: ivanpardo/ssh23:base
	privileged: true
	container_name: ssh.edt.org
	hostname: ssh.edt.org
	ports:
  	- 2022:20
	networks:
  	- mynet
networks:
  mynet:
```
Comanda per copiar els fitxers del prova1 a l'usuari anna Remot
```
rsync -a dir1/ anna@172.26.0.2:/tmp/home/anna/.

```
Ara hauriem de fer ls dins del container per comprobar la sincronització

## Remot - Local

Dins del container crearem el bulk de dades per fer la prova(directori home usuari):
```
touch fitxers_anna{1..30}
```
I des de el nostre ordinador farem:
```
rsync -a anna@172.26.0.2:/tmp/home/anna/ .
```
I ara només hauriem de fer ls -l per comprobar-ho tot

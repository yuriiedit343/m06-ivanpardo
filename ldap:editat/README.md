# LDAP server:editat Ivan Pardo

### Afegir el fitxer Dockerfile

~~~
# ldapserver 2022
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver 2022"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
~~~

### Afegir el startup.sh
~~~
#! /bin/bash

# export DEBIAN_FRONTEND=noninteractive
# apt-get -y install slapd

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
~~~

### Afegim el fitxer .ldif per afegir els usuaris

~~~
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=grupclase,dc=edt,dc=org
ou: clase
description: exemple ou
objectclass: organizationalunit

dn: ou=grupclase2,dc=edt,dc=org
ou: clase2
description: exemple ou2
objectclass: organizationalunit

dn: uid=user01,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user1
cn: user01
sn: user01
homephone: 555-222-2220
mail: user01@edt.org
description: exemple user 01
ou: grupclase
uid: user01
uidNumber: 5000
gidNumber: 100
homeDirectory: /tmp/home/user01
userPassword: user01

dn: uid=user02,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user2
cn: user02
sn: user02
homephone: 555-222-2220
mail: user02@edt.org
description: exemple user 02
ou: grupclase
uid: user02
uidNumber: 5001
gidNumber: 101
homeDirectory: /tmp/home/user02
userPassword: user02

dn: uid=user03,ou=grupclase,dc=edt,dc=org
cn: user3
cn: user03
sn: user03
homephone: 555-222-2220
mail: user03@edt.org
description: exemple user 03
ou: grupclase
uid: user03
uidNumber: 5003
gidNumber: 103
homeDirectory: /tmp/home/user03
userPassword: user03


dn: uid=user04,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user4
cn: user04
sn: user04
homephone: 555-222-2220
mail: user04@edt.org
description: exemple user 04
ou: grupclase
uid: user04
uidNumber: 5004
gidNumber: 104
homeDirectory: /tmp/home/user04
userPassword: user04


dn: uid=user05,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user5
cn: user05
sn: user05
homephone: 555-222-2220
mail: user05@edt.org
description: exemple user 05
ou: grupclase
uid: user05
uidNumber: 5005
gidNumber: 105
homeDirectory: /tmp/home/user05
userPassword: user05


dn: uid=user06,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user6
cn: user06
sn: user06
homephone: 555-222-2220
mail: user06@edt.org
description: exemple user 06
ou: grupclase
uid: user06
uidNumber: 5006
gidNumber: 106
homeDirectory: /tmp/home/user06
userPassword: user06

dn: uid=user07,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user7
cn: user07
sn: user07
homephone: 555-222-2220
mail: user07@edt.org
description: exemple user 07
ou: grupclase
uid: user07
uidNumber: 5007
gidNumber: 107
homeDirectory: /tmp/home/user07
userPassword: user07

dn: uid=user08,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user8
cn: user08
sn: user08
homephone: 555-222-2220
mail: user08@edt.org
description: exemple user 08
ou: grupclase
uid: user08
uidNumber: 5008
gidNumber: 108
homeDirectory: /tmp/home/user08
userPassword: user08

dn: uid=user09,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user9
cn: user09
sn: user09
homephone: 555-222-2220
mail: user09@edt.org
description: exemple user 09
ou: grupclase
uid: user09
uidNumber: 5009
gidNumber: 109
homeDirectory: /tmp/home/user09
userPassword: user09

dn: uid=user10,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user10
cn: user10
sn: user10
homephone: 555-222-2220
mail: user10@edt.org
description: exemple user 10
ou: grupclase
uid: user10
uidNumber: 5010
gidNumber: 110
homeDirectory: /tmp/home/user10
userPassword: user10

dn: uid=user11,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user11
cn: user11
sn: user11
homephone: 555-222-2220
mail: user11@edt.org
description: exemple user 11
ou: grupclase
uid: user11
uidNumber: 5011
gidNumber: 111
homeDirectory: /tmp/home/user11
userPassword: user11


dn: uid=5012,ou=grupclase2,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: pepe
cn: pepe
sn: pepe
homephone: 555-222-2220
mail: pepe@edt.org
description: exemple pepe
ou: grupclase2
uid: pepe
uidNumber: 5012
gidNumber: 112
homeDirectory: /tmp/home/pepe
userPassword: pepe

dn: uid=5013,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: eduard
cn: eduard
sn: eduard
homephone: 555-222-2220
mail: eduard@edt.org
description: exemple eduard
ou: grupclase2
uid: user01
uidNumber: 5013
gidNumber: 113
homeDirectory: /tmp/home/eduard
userPassword: eduard
~~~

###UN cop tenim aixo, hem de copiar el fitxer de configuracio slapd.conf
## Per afegir la contrasenya encriptada hem de fer la seguent comanda:
~~~
slappasswd -h {CRYPT} -s secret
~~~

### Per ultim afegim un modify.ldif per comprobar que funcioni l'usuari admin
~~~
dn: uid=5013,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: mail
mail: pepe@gmail.com
~~~

## Per executar la maquina fem:

~~~
docker run --rm --name ldap.edt.org -h ldap.edt.org  -p 389:389 -d ldap22:edit
~~~

## Comprobacions:

~~~
ldapmodify -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f modify.ldif
~~~


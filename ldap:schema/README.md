# Ldap Schema Ivan Pardo

## Crear schema amb objecte structural i auxiliary

```
objectClass (1.1.2.1.1 NAME 'x-animals'
 DESC 'Caracteristiques animals'
 STRUCTURAL
 MUST ( x-nom $ x-foto)
 MAY ( x-caracteristiques $ x-carnivor )
 )

objectClass (1.1.2.2.2 NAME 'x-perill-extincio'
 DESC 'informacio sobre el perill de les especies'
 AUXILIARY
 MUST ( x-en-perill ) 
 MAY ( x-cuantitat-especies $ x-habitat ) 
 )
```

## Atributs per a cada objecte

```
attributetype (1.1.2.1.1 NAME 'x-nom'
  DESC 'Nom del animal'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE
 )

attributetype (1.1.2.1.2 NAME 'x-foto'
  DESC 'Foto del animal'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28
 )

attributetype (1.1.2.1.3 NAME 'x-caracteristiques'
  DESC 'PDF amb les caracteristiques basiques del animal'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
 )

attributetype (1.1.2.1.4 NAME 'x-carnivor'
  DESC 'boolean de si es carnivor o no'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE
 )

attributetype (1.1.2.2.1 NAME 'x-cuantitat-especies'
 DESC 'cuantitat despecies vives'
 EQUALITY integerMatch
 ORDERING integerOrderingMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
 SINGLE-VALUE
 )

attributetype (1.1.2.2.2 NAME 'x-en-perill'
 DESC 'boolean de si es un animal en perill dextincio o no'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
 SINGLE-VALUE
 )
attributetype (1.1.2.2.3 NAME 'x-habitat'
 DESC 'on esta situat naturalment lanimal'
 EQUALITY caseIgnoreMatch
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE
 )
```

## Crear Arxiu amb les dades

```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org
#---------PracticaSchema-------------------------------------

dn: ou=practica,dc=edt,dc=org
ou: practica
description: grup per a la practica
objectClass: organizationalUnit



dn: x-nom=oso panda,ou=practica,dc=edt,dc=org
objectClass: x-animals
objectClass: x-perill-extincio
x-nom: oso panda
x-foto:< file:/opt/docker/osopanda.jpeg
x-caracteristiques:< file:/opt/docker/osopanda.pdf
x-carnivor: FALSE
x-en-perill: TRUE
x-cuantitat-especies: 2000

dn: x-nom=carpa,ou=practica,dc=edt,dc=org
objectClass: x-animals
x-nom: carpa
x-foto:< file:/opt/docker/carpa.jpeg
x-caracteristiques:< file:/opt/docker/carpa.pdf
x-carnivor: FALSE


dn: x-nom=tigre,ou=practica,dc=edt,dc=org
objectClass: x-animals
objectClass: x-perill-extincio
x-nom: tigre
x-foto:< file:/opt/docker/tigre.jpeg
x-caracteristiques:< file:/opt/docker/tigre.pdf
x-carnivor: TRUE
x-en-perill: TRUE
x-cuantitat-especies: 4500
```

## Esborrar schemas no necesaris en arxiu de configuracio ldap

```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
include		/opt/docker/practica.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```

## Crear Docker file

```
# ldapserver 2022

FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL subject="ldapserver 2022"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y procps iproute2 iputils-ping nmap tree slapd ldap-utils
RUN mkdir /opt/docker/
WORKDIR /opt/docker/
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
CMD /opt/docker/startup.sh
EXPOSE 389
```
## Desplegar maquines php i ldap amb docker compose

docker compose -f docker-compose.yml up -d
```

  # Asegúrate de utilizar la versión correcta de Docker Compose
version: "3.1"
services:
  ldap:
    image: ivanpardo/schema:schema
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - mynet

  phpldapadmin:
    image: edtasixm06/ldap22:phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - mynet

networks:
  mynet:
``

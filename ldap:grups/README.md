# Ldap23--IvanPardo--GRUPS

## Afegir una nova ou grups

```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Nou grup anomenat grups per a usuaris linux
objectclass: organizationalunit
```
### Cal canviar en l'arxiu .ldif tots els dn, hem de canviar el cn pel uid
```
Exemple:
dn: uid=pere,ou=usuaris,dc=edt,dc=org
```


## Definir els nous grups

```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors
memberUid: pau
memberUid: pere
memberUid: jordi

dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup de alumnes
memberUid: anna
memberUid: marta
memberUid: jordi

dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Grup de 1asix
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05  

dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup de 2asix
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10

dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: wheel
gidNumber: 27
description: Grup de sudo
memberUid: admin

dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup de 1wiam

dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup de 2wiam

dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup de 1hiaw
```

## Verificacio de les dades

```
slapcat
ldapsearch -x -LLL -b 'dc=edt,dc=org' | grep dn
ldapsearch -x -LLL -b 'dc=edt,dc=org' 'uid=nomuser'
ldapsearch -x -LLL -b 'dc=edt,dc=org' 'cn=nomgrup'
```




/etc/samba/smb.conf

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.001.jpeg)

Exemple 1:

guest ok = yes

Permet l’accés al share de usuaris anònims, sense identificar. és equivalent a la opció public = yes

Observar que l’accés a disc de l’usuari anònim guest es transforma (id mapping) en l’usuari unix nobody.

Afegir una carpeta en /etc/samba/smb.conf

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.002.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.003.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.004.png)

Exemple 2: Només usuari guest

guest only = yes

Permet únicament accedir al recurs via usuari anònim. No es permet l’accés via usuari identificat.

Observem que el sistema ens enganya, ens diu que ens deixa entrar com l’usuari lila però en realitat som nobody.

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.005.png)

NOTAAAAAAAAAAAAAAAAAAA PER CREAR USUARIS TAN A CLIENT COM A SERVER for user in lila roc patipla pla

do

useradd -m -s /bin/bash $user

echo -e "$user\n$user" | smbpasswd -a $user

done

pdbedit -L

—------------------------------------------------------------------------

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.006.png)

Encara que hagi entrar com a lila, en realitat, som nobody

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.007.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.008.png)

mira com pepe es nobody

Exemple 3: Usuari només guest amb idmap a un compte unix (deprecated guest account?)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.009.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.010.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.011.png)

Exemple 4: Usuari identificat

Les ordres client samba permeten indicar en nom de quin usuari es vol realitzar la connexió. El

password es pot demanar interactivament o indicar-lo en la línia de comanda. Així per exemple

amb la ordre smbclient podem fer:

$ smbclient -U user //server/recurs

$ smbclient -U user%password //server/recurs

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.012.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.013.png)

Exemple 5: Valid users

valid users = user1 user2 userN

Permet indicar la llista d’usuaris vàlids per accedir al recurs. La resta d’usuaris no podran accedir-hi. Tampoc guest tot i que s’hagi indicat guest ok.

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.014.png)

Veiem con lila no pot

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.015.png)

i els usuaris patipla i roc si que poden

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.016.png)

Exemple 6: Invalid users

invalid users = user1 user2 userN

Indica la llista d’usuaris que no tenen permès accedir al recurs. La resta d’usuaris vàlids si hi poden accedir (guest dependrà de si s¡ha permès o no via guest ok).

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.017.png)

Ara el resultat es al reves que l’exercici 5

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.018.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.019.png)

Exemple 7: Admin users

admin users = user1 user2 userN

Permet definir un conjunt d’usuaris samba que seran convertits (id mapping) a l’usuari root. És a dir, estem dient que tal i tal usuari samba quan accedeixi a disc al recurs ha d’actuar com a usuari administrador (root en cas de unix).

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.020.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.021.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.022.png)

Exemple 8: Recurs de només lectura

read only = yes

writable = no

Els recursos es poden configurar per ser de només lectura (són equivalents)

Exemple 9: Recurs de lectura/escriptura

read only = no

writable = yes

Recursos configurats de lectura/escriptura. Són equivalents.

Exemple 10: Llista d’usuaris autoritzats de lectura

read list = user1 user2 userN

Indica la llista d’usuaris que només poden llegir. Atenció, aquesta directiva s’indica en recursos que són de lectura/escriptura i serveix per restringir aquests usuaris atorgant-los només el dret de lectura (i privant-los del de escriptura)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.023.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.024.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.025.png)

Exemple 11: Llista d’usuaris autoritzats per a escriptura

write list = user1 user2 userN

Indica la llista d’usuaris amb dret d’escriptura al recurs. S’utilitza en recursos que són read only per a tots els usuaris però es permet als usuaris indicats a la llista el dret de escriptura

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.026.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.027.png)

Exemple 12: Modes de directori i fitxer

create mode = mode

directory mode = mode

Permeten establir els permisos dels nous directoris i fitxers creats dins la carpeta

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.028.png)

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.029.png)

ls -la /var/lib/samba/public

![](images/Aspose.Words.6bf07c88-2a66-4d3a-abcf-430d182eb761.030.png)
